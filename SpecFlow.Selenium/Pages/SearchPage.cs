﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Reflection;

namespace SpecFlow.Selenium.Pages
{
	public class SearchPage
	{
		private readonly IWebDriver driver;
        public readonly Dictionary<string, object> pageMap;

		public SearchPage(IWebDriver driver)
		{
			this.driver = driver;
            this.pageMap = GetPage();
		}

		public void PerformSearch(string searchText)
		{
			
		}

        public IWebElement GetElement(string element){
            object value = "";
            pageMap.TryGetValue(element,out value);
            return driver.FindElement(By.CssSelector(value.ToString()));
        }

        public IList<IWebElement> GetElements(string elements)
        {
            object value = "";
            pageMap.TryGetValue(elements, out value);
            return driver.FindElements(By.CssSelector(value.ToString()));
        }

		//public void SelectResult(string expResult)
		//{
		//	IWebElement link = FindResult(expResult);
		//	Assert.IsNotNull(link, $"Could not find link for: {expResult}");
		//	link.Click();
		//}

		//private IWebElement FindResult(string expResult)
		//{
		//	foreach (IWebElement link in searchResults)
		//	{
		//		if (link.Text.ToUpper().Contains(expResult.ToUpper())) {
		//			return link;
		//		}
		//	}
		//	return null;
		//}

        Dictionary<string, object> GetPage()
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            Stream pageMapStream = assembly.GetManifestResourceStream("Specflow.Selenium.Pages.searchPage.json");
            if (pageMapStream == null)
                throw new FileNotFoundException("Could not find page json resource");

            StreamReader reader = new StreamReader(pageMapStream);
            JObject obj = JObject.Parse(reader.ReadToEnd());

            Dictionary<string, object> page = new Dictionary<string, object>();
            page = obj.ToObject<Dictionary<string, object>>();

            return page;
        }
	}
}
